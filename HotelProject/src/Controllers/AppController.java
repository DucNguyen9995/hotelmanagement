/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import SQL.*;
import SQL.SQLService;
import Views.CreateNewAccountBox;
import Views.LoginWindow;
import Views.Menu_chuc_nang;
import Views.menuUpdate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author user
 */
public class AppController {
    
    public static boolean running;
    public String strIDNhanVien;
    public String strTenNhanVien;
    
    private static SQLService sqlService;
    
    
    // <editor-fold defaultstate="collapsed" desc="AppController getInstance">
    private AppController() {
        this.running = true;
        sqlService = SQLService.getInstance();
//        sqlService.testDatabase();
        this.bootUp();
    }
    
    public static AppController getInstance() {
        return AppControllerHolder.INSTANCE;
    }
    
    private static class AppControllerHolder {
        private static final AppController INSTANCE = new AppController();
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Boot Up Sequence">
    private void bootUp() {
        Vector vectTables = new Vector();
        vectTables.add(SQLQuery.SQLTABLENHANVIEN);
        SQLQuery query = SQLQuerySelect.queryWithVectorColumns(null, vectTables);
        try {
            if (query instanceof SQLQuerySelect) {
                ResultSet resultSet = sqlService.executeQueryWithQuerySelect(query);
                if (!resultSet.next()) {
                    CreateNewAccountBox firstBox = new CreateNewAccountBox(null);
                } else {
                    LoginWindow login = new LoginWindow();
                }
            }
        } catch (SQLException ex) {
            
        }
    }
    // </editor-fold>
    
    public void accessGranted() {
        // TODO handling App flow after accessed the Database
        System.out.println(strIDNhanVien);
        System.out.println(strTenNhanVien);
        Menu_chuc_nang menu_chuc_nang = new Menu_chuc_nang("Yolo");
    }
}
