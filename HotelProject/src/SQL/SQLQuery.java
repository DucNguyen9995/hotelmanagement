/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author user
 */
public abstract class SQLQuery {
    // <editor-fold defaultstate="collapsed" desc="Không cần quan tâm">
    // <editor-fold defaultstate="" desc="Query Constant">
    // Constants
    protected static final String SQLQUERYKEYADD = "add ";
    protected static final String SQLQUERYKEYFROM = "from ";
    protected static final String SQLQUERYKEYAS = "as ";
    protected static final String SQLQUERYKEYAVG = "avg ";
    protected static final String SQLQUERYKEYBETWEEN = "between ";
    protected static final String SQLQUERYKEYCREATETABLE = "create table ";
    protected static final String SQLQUERYKEYGROUPBY = "group by ";
    protected static final String SQLQUERYKEYLIKE = "like ";
    protected static final String SQLQUERYKEYLIMIT = "limit ";
    protected static final String SQLQUERYKEYMAX = "max ";
    protected static final String SQLQUERYKEYMIN = "min ";
    protected static final String SQLQUERYKEYROUND = "round ";
    protected static final String SQLQUERYKEYSUM = "sum ";
    protected static final String SQLQUERYKEYSET = "set ";
    protected static final String SQLQUERYKEYINNERJOIN = "inner join ";
    protected static final String SQLQUERYKEYVALUES = "values ";
    
    // <editor-fold defaultstate="Collapsed" desc="Các toán tử của lệnh SQL">
    protected static final String SQLQUERYKEYIN = "in ";
    protected static final String SQLQUERYKEYAND = "and ";
    protected static final String SQLQUERYKEYOR = "or ";
    
    // </editor-fold>
    
    // Query Where part
    protected static final String SQLQUERYKEYWHERE = "where ";
    
    // Query OrderBy part
    protected static final String SQLQUERYKEYORDERBY = "order by ";
    protected static final String SQLQUERYKEYORDERBYASC = "asc ";
    protected static final String SQLQUERYKEYORDERBYDESC = "desc ";
    
    // <editor-fold defaultstate="Collapsed" desc="Query Type">
    // QueryType
    public static final String SQLQUERYTYPESELECT = "Select";
    public static final String SQLQUERYTYPEDELETE = "Delete";
    public static final String SQLQUERYTYPEINSERT = "Insert";
    public static final String SQLQUERYTYPEUPDATE = "Update";
    // </editor-fold>
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table's Thing">
    // TableID
    public static final String SQLTABLEHOADON = "Hoa_Don";
    public static final String SQLTABLENHANVIEN = "Nhan_Vien";
    public static final String SQLTABLEKHACHHANG = "Khach_Hang";
    public static final String SQLTABLEDATPHONG = "Dat_Phong";
    public static final String SQLTABLECHITIETDICHVU = "Chi_Tiet_Dich_Vu";
    public static final String SQLTABLECHITIETDATPHONG = "Chi_Tiet_Dat_Phong";
    public static final String SQLTABLEDICHVU = "Dich_Vu";
    public static final String SQLTABLEPHONGKHACHSAN = "Phong_Khach_San";
    
    // TableCols for Bảng Hóa đơn
    public static final String SQLTABLEHOADONIDHOADON = "ID_Hoa_Don";
    public static final String SQLTABLEHOADONNGAYLAP = "Ngay_Lap";
    public static final String SQLTABLEHOADONMADATPHONG = "Ma_Dat_Phong";
    public static final String SQLTABLEHOADONTONGTHANHTOAN = "Tong_Thanh_Toan";
    
    // TableCols for Bảng nhân viên
    public static final String SQLTABLENHANVIENIDNHANVIEN = "ID_Nhan_Vien";
    public static final String SQLTABLENHANVIENTENNHANVIEN = "Ten_Nhan_Vien";
    public static final String SQLTABLENHANVIENUSERNAME = "Username";
    public static final String SQLTABLENHANVIENPASSWORD = "Password";
    public static final String SQLTABLENHANVIENSODIENTHOAI = "So_Dien_Thoai";
    public static final String SQLTABLENHANVIENDIACHI = "Dia_Chi";
    
    // TableCols for bảng khách hàng
    public static final String SQLTABLEKHACHHANGIDKHACHHANG = "ID_Khach_Hang";
    public static final String SQLTABLEKHACHHANGTENKHACHHANG = "Ten_Khach_Hang";
    public static final String SQLTABLEKHACHHANGCMTND = "CMTND";
    public static final String SQLTABLEKHACHHANGSODIENTHOAI = "So_Dien_Thoai";
    public static final String SQLTABLEKHACHHANGDIACHI = "Dia_Chi";
    
    // TableCols for bảng đặt phòng
    public static final String SQLTABLEDATPHONGIDKHACHHANG = "ID_Khach_Hang";
    public static final String SQLTABLEDATPHONGNGAYDAT = "Ngay_Dat";
    public static final String SQLTABLEDATPHONGNGAYNHAN = "Ngay_Nhan";
    public static final String SQLTABLEDATPHONGMADATPHONG = "Ma_Dat_Phong";
    public static final String SQLTABLEDATPHONGDATCOC = "Dat_Coc";
    public static final String SQLTABLEDATPHONGIDNHANVIEN = "ID_Nhan_Vien";
    
    // TableCols for bảng chi tiết dịch vụ
    public static final String SQLTABLECHITIETDICHVUMADATPHONG = "Ma_Dat_Phong";
    public static final String SQLTABLECHITIETDICHVUIDDICHVU = "ID_Dich_Vu";
    public static final String SQLTABLECHITIETDICHVUSOLUONG = "So_Luong";
    
    // TableCols for bảng chi tiết đặt phòng
    public static final String SQLTABLECHITIETDATPHONGMADATPHONG = "Ma_Dat_Phong";
    public static final String SQLTABLECHITIETDATPHONGSOHIEU = "So_Hieu";
    public static final String SQLTABLECHITIETDATPHONGNGAYTRA = "Ngay_Tra";
    
    // TableCols for bảng dịch vụ
    public static final String SQLTABLEDICHVUIDDICHVU = "ID_Dich_Vu";
    public static final String SQLTABLEDICHVUTENDICHVU = "Ten_Dich_Vu";
    public static final String SQLTABLEDICHVUGIADICHVU = "Gia_Dich_Vu";
    
    // TableCols for bảng phòng khách sạn
    public static final String SQLTABLEPHONGKHACHSANSOHIEU = "So_Hieu";
    public static final String SQLTABLEPHONGKHACHSANKIEUPHONG = "Kieu_Phong";
    public static final String SQLTABLEPHONGKHACHSANLOAIPHONG = "Loai_Phong";
    public static final String SQLTABLEPHONGKHACHSANGIAPHONG = "Gia_Phong";
    public static final String SQLTABLEPHONGKHACHSANTINHTRANG = "Tinh_Trang";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Thuộc tính của SQLQuery">
    
    // <editor-fold defaultstate="collapsed" desc="Thuộc tính tự thêm">
    // Properties
    protected String queryType;
    protected String queryStatement;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Các lựa chọn thêm vào của các câu lệnh SQL">
    // Optional Parts of query
    public HashMap dictWhereField;
    public boolean order;
    public String orderType;
    
    // </editor-fold>
    // </editor-fold>
    
    protected SQLQuery(String queryType) {
        this.queryType = queryType;
    }
    
    public abstract String statementFromQuery();
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Thêm các thuộc tính cho các phần của câu lệnh SQL - Dùng cẩn thận">
    public void setOrder(boolean order, String orderType) {
        this.order = order;
        this.orderType = orderType;
    }
    
    public void setWhere(HashMap<String, Vector<String>> dictWhereField) {
        this.dictWhereField = dictWhereField;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Parse statements parts(where fields, ...)">
    public void statementWithParts() {
        if(dictWhereField != null) {
            this.queryStatement = this.queryStatement.concat(" " + SQLQUERYKEYWHERE);
            Set setKeys = this.dictWhereField.keySet();
            for (Object strKey : setKeys) {
                this.queryStatement = this.queryStatement.concat((String)strKey + " ");
                this.queryStatement = this.queryStatement.concat(SQLQUERYKEYIN + "(");
                for(Object strKeyValue : (Vector)this.dictWhereField.get(strKey)) {
                    if (strKeyValue != ((Vector)this.dictWhereField.get(strKey)).lastElement()) {
                        this.queryStatement = this.queryStatement.concat("'" + (String)strKeyValue + "',");
                    } else {
                        this.queryStatement = this.queryStatement.concat("'" + (String)strKeyValue + "'");
                    }
                }
                this.queryStatement = this.queryStatement.concat(")");
            }
        }
    }
}
