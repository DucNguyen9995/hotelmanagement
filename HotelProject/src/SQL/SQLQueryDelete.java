/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.Vector;

/**
 *
 * @author user
 */
public class SQLQueryDelete extends SQLQuery {
    
    private static final String SQLQUERYKEYDELETEFROM = "delete from ";
    
    private Vector tables;
    
    private SQLQueryDelete(String queryType) {
        super(queryType);
    }
    
    public static SQLQueryDelete queryWithVectorTables(Vector tables) {
        SQLQueryDelete queryDelete = new SQLQueryDelete(SQLQUERYTYPEDELETE);
        queryDelete.tables = tables;
        return queryDelete;
    }
    
    @Override
    public String statementFromQuery() {
        String statementQuery = SQLQUERYKEYDELETEFROM;
        if (this.tables == null){
            System.out.println("Table Empty is not allowed");
            return null;
        } else {
            for (Object strTable : this.tables) {
                if (strTable == this.tables.lastElement()) {
                    statementQuery = statementQuery.concat((String)strTable + " ");
                    break;
                }
                statementQuery = statementQuery.concat((String)strTable + " " + SQLQUERYKEYINNERJOIN + " ");
            }
        }
        this.queryStatement = statementQuery;
        this.statementWithParts();
        return this.queryStatement;
    }
    
    public static void main(String []args) {
        Vector vectTable = new Vector();
        vectTable.add(SQLQuery.SQLTABLENHANVIEN);
        SQLQuery query = SQLQueryDelete.queryWithVectorTables(vectTable);
        SQLService.getInstance().executeQueryWithQuery(query);
//        SQLQuery select = SQLQuerySelect.queryWithVectorColumns(null , vectTable, null, true, vectTable);
//        try {
//            ResultSet rs = SQLService.getInstance().executeQueryWithQuerySelect(query);
//            while (rs.next()) {
//                System.out.println(rs.getString(1));
//            }
//            System.out.println("Work!");
//        } catch (SQLException ex) {
//            Logger.getLogger(SQLQueryDelete.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
    }
}
