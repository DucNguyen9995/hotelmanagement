/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author user
 */
public class SQLQueryInsert extends SQLQuery {

    protected static final String SQLQUERYKEYINSERTINTO = "insert into ";
    
    private String tableName;
    private Vector tableColumns;
    private HashMap tableData;
    
    // <editor-fold defaultstate="collapsed" desc="Constructor">
    private SQLQueryInsert(String queryType) {
        super(queryType);
    }
    
    public static SQLQueryInsert queryWithVectorColumns(String tableName, Vector tableColumns, HashMap tableData) {
        SQLQueryInsert queryInsert = new SQLQueryInsert(SQLQUERYTYPEINSERT);
        queryInsert.tableName = tableName;
        queryInsert.tableColumns = tableColumns;
        queryInsert.tableData = tableData;
        return queryInsert;
    }
    // </editor-fold>
    
    @Override
    public String statementFromQuery() {
        String statementQuery = SQLQUERYKEYINSERTINTO;
        statementQuery = statementQuery.concat(this.tableName + "(");
        for (Object strColumns : this.tableColumns) {
            if (strColumns == this.tableColumns.lastElement()) {
                statementQuery = statementQuery.concat((String)strColumns + ")");
                break;
            }
            statementQuery = statementQuery.concat((String)strColumns + ", ");
        }
        statementQuery = statementQuery.concat(SQLQUERYKEYVALUES + "(");
        for (Object strColumns : this.tableColumns) {
            if (strColumns == this.tableColumns.lastElement()) {
                statementQuery = statementQuery.concat("'" + (String)this.tableData.get(strColumns) + "')");
                break;
            }
            statementQuery = statementQuery.concat("'" + (String)this.tableData.get(strColumns) + "',");
        }
        this.statementWithParts();
        return statementQuery;
    }
}
