/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author user
 */
public class SQLQueryUpdate extends SQLQuery {
    
    protected static final String SQLQUERYKEYUPDATE = "update ";
    protected static final String SQLQUERYKEYSET = "set ";
    
    public String table;
    public Vector vectInsertTableKeys;
    public HashMap dictInsertValues;
    
    public static SQLQueryUpdate queryWithVectorColumnAsInsertTableKey(String table, Vector vectInsertTableKeys, HashMap dictInsertValues) {
        SQLQueryUpdate query = new SQLQueryUpdate(SQLQuery.SQLQUERYTYPEUPDATE);
        query.table = table;
        query.vectInsertTableKeys = vectInsertTableKeys;
        query.dictInsertValues = dictInsertValues;
        return query;
    }
    
    @Override
    // E.g: update Nhan_Vien set ID_Nhan_Vien = 'Yolo'
    public String statementFromQuery() {
        String statementQuery = SQLQUERYKEYUPDATE;                                                       // update ...
        if (this.table == null) {
            System.out.println("Table field = null not allowed");
            return null;
        }
        statementQuery = statementQuery.concat(this.table + " ");                                        // Nhan_Vien ...
        statementQuery = statementQuery.concat(SQLQuery.SQLQUERYKEYSET);                                 // set ...
        for (Object strKey : this.vectInsertTableKeys) {
            statementQuery = statementQuery.concat((String)strKey + " = ");                              // ID_NhanVien = ...
            if (strKey != this.vectInsertTableKeys.lastElement()) {
                statementQuery = statementQuery.concat("'" + (String)this.dictInsertValues.get(strKey) + "', ");     // 'Yolo', ...
            } else {
                statementQuery = statementQuery.concat("'" + (String)this.dictInsertValues.get(strKey) + "'");
            }
        }
        this.queryStatement = statementQuery;
        this.statementWithParts();
        return this.queryStatement;
    }

    private SQLQueryUpdate(String queryType) {
        super(queryType);
    }
    
    public static void main(String []args) {
        Vector vectInsertTableColumnKeys = new Vector();
        vectInsertTableColumnKeys.add(SQLQuery.SQLTABLENHANVIENTENNHANVIEN);
        HashMap dictInsertValues = new HashMap();
        dictInsertValues.put(SQLQuery.SQLTABLENHANVIENTENNHANVIEN, "Cái đm Hiếu Chó");
        HashMap dictWhereField = new HashMap();
        Vector vectWhereVals = new Vector();
        vectWhereVals.add("Yolo");
        dictWhereField.put(SQLTABLEHOADONIDHOADON, vectWhereVals);
        SQLQuery query = SQLQueryUpdate.queryWithVectorColumnAsInsertTableKey(SQLQuery.SQLTABLENHANVIEN, vectInsertTableColumnKeys, dictInsertValues);
        query.setWhere(dictWhereField);
        System.out.println(query.statementFromQuery());
    }
}
