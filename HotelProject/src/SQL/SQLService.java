/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class SQLService {
    
    
    private static final String SQLDATABASELINK = "jdbc:odbc:Hotel";
    
    // <editor-fold defaultstate="collapsed" desc="connection & test">
    private SQLService() {
        try {
            DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
        } catch (Exception e) {
            
        }        
    }
    
    // <editor-fold defaultstate="collapsed" desc="junk tester">
    public void testSourceTree() {
        
    }
    
    public void testDatabase() {
        try {
            Statement statement = SQLService.getInstance().connectToDatabase().createStatement();
            System.out.println("Work!");
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // </editor-fold>
    
    public static SQLService getInstance() {
        return SQLServiceHolder.INSTANCE;
    }
    
    private static class SQLServiceHolder {
        private static final SQLService INSTANCE = new SQLService();
    }
    
    public Connection connectToDatabase() {
        try {
            return DriverManager.getConnection(SQLDATABASELINK);
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Couldn't connect to the database");
        return null;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Các hàm gửi request">
    public ResultSet executeQueryWithQuerySelect(SQLQuery query) {
        String statementQuery;
        statementQuery = query.statementFromQuery();
        ResultSet resultSet = null;
        try {
            Statement statement = this.connectToDatabase().createStatement();
            if (query instanceof SQLQuerySelect)
                resultSet = statement.executeQuery(statementQuery);
            else {
                statement.executeQuery(statementQuery);
            }
            return resultSet;
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultSet;
    }
    
    public int executeQueryWithQuery(SQLQuery query) {
        String statementQuery;
        statementQuery = query.statementFromQuery();
        try {
            Statement statement = this.connectToDatabase().createStatement();
            int notError = statement.executeUpdate(statementQuery);
            return notError;
        } catch (SQLException ex) {
            Logger.getLogger(SQLService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    // </editor-fold>
    
    public  ResultSet findCriteriaInTable(HashMap<String, Vector<String>> criteria, Vector<String>Table) {
        ResultSet resultSet = null;
        return resultSet;
    }
}

//dbcc checkident ('BookTable', reseed, 0) 
//go