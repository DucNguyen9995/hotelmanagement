/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SupportedFiles;

import SQL.SQLService;
import java.util.Arrays;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.*;

/**
 *
 * @author user
 */
public class ExcelReaderAndCreator {

    /**
     *
     * @param table                 the Table String: SQLTABLE... 
     * @param tableIDColumn         the Table Column to generate ID...
     * @param vectColumn            the Vector for The column to insert...
     */
    public static void readDataFromXLSXIntoTable(final String table, final String tableIDColumn, final Vector vectColumn) {
        JFileChooser fileChooser = new JFileChooser();
        FileFilter fileFilter = new FileNameExtensionFilter("Excel 97 - 2003 Workbook", "xls");
        fileChooser.setFileFilter(fileFilter);
        fileFilter = new FileNameExtensionFilter("Excel Workbook ", "xlsx");
        fileChooser.addChoosableFileFilter(fileFilter);
        int i = fileChooser.showOpenDialog(null);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file != null && file.canRead()) {
                Vector<String> vectFileParts = new Vector<String>(Arrays.asList(file.getName().split("\\.")));
                String fileType = (String)vectFileParts.lastElement();
                if (fileType.equals("xls") || fileType.equals("xlsx")) {
                    FileInputStream fileInputStream = null;
                    try {
                        fileInputStream = new FileInputStream(file);
                        if (fileType.equals("xls")) {
                            HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
                            
                            if (workbook != null) {
                                HSSFSheet sheet = workbook.getSheetAt(0);
                                
                                if (sheet != null && sheet.getLastRowNum() > 0) {
                                    for (int index = 1; i < sheet.getLastRowNum(); i++) {
                                        final Row row = sheet.getRow(index);
                                        new Thread(new Runnable(){

                                            @Override
                                            public void run() {
                                                String colString = vectColumn.toString();
                                                colString = colString.replaceAll("\\[", "");
                                                colString = colString.replaceAll("\\]", "");
                                                String sqlCmd = "insert into " + table + "(" + tableIDColumn + "," + colString + ") values ('" + AppDelegate.autoGenerateIDForTable(table, tableIDColumn) + "'";
                                                for (int j = 0; j < vectColumn.size(); j++) {
                                                    Cell cell = row.getCell(j);
                                                    
                                                    if (j == vectColumn.size() - 1) {
                                                        sqlCmd = sqlCmd.concat(",'" + cellValueToString(cell) +"')");
                                                        break;
                                                    }
                                                    sqlCmd = sqlCmd.concat(",'" + cellValueToString(cell) + "'");
                                                }
                                                try {
                                                    SQLService.getInstance().connectToDatabase().createStatement().executeUpdate(sqlCmd);
                                                } catch (SQLException ex) {
                                                    Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                            }
                                        }).start();
                                    }
                                }
                            }
                            workbook.close();
                        }
                        if (fileType.equals("xlsx")) {
                            XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
                            
                            if (workbook != null) {
                                XSSFSheet sheet = workbook.getSheetAt(0);
                                
                                if (sheet != null && sheet.getLastRowNum() > 0) {
                                    for (int index = 1; index <= sheet.getLastRowNum(); index++) {
                                        final Row row = sheet.getRow(index);
                                        new Thread(new Runnable(){

                                            @Override
                                            public void run() {
                                                String colString = vectColumn.toString();
                                                colString = colString.replaceAll("\\[", "");
                                                colString = colString.replaceAll("\\]", "");
                                                String sqlCmd = "insert into " + table + "(" + tableIDColumn + "," + colString + ") values ('" + AppDelegate.autoGenerateIDForTable(table, tableIDColumn) + "'";
                                                for (int j = 0; j < vectColumn.size(); j++) {
                                                    Cell cell = row.getCell(j);
                                                    
                                                    if (j == vectColumn.size() - 1) {
                                                        sqlCmd = sqlCmd.concat(",'" + cellValueToString(cell) +"')");
                                                        break;
                                                    }
                                                    sqlCmd = sqlCmd.concat(",'" + cellValueToString(cell) + "'");
                                                }
                                                try {
                                                    SQLService.getInstance().connectToDatabase().createStatement().executeUpdate(sqlCmd);
                                                } catch (SQLException ex) {
                                                    Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                            }
                                        }).start();
                                    }
                                }
                            }
                            workbook.close();
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            fileInputStream.close();
                        } catch (IOException ex) {
                            Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
    }
    
    private static String cellValueToString(Cell cell) {
        String cellString = null;
        
        if (cell == null)
            cellString = "";
        else
            switch(cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    cellString = cell.getStringCellValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        Date date = cell.getDateCellValue();
                        cellString = new SimpleDateFormat("yyyy-MM-dd").format(new java.sql.Date(date.getTime()));
                        break;
                    }
                    cellString = ("" + cell.getNumericCellValue()).split("\\.")[0];
                    break;
                case Cell.CELL_TYPE_BLANK:
                    cellString = "";
                    break;

            }

        return cellString;
    }
    
    /**
     *
     * @param table
     * @param vectColumn
     */
    public static void createStandardXLSXFile(final String table, final Vector vectColumn) {
        JFileChooser fileChooser = new JFileChooser();
        FileFilter fileFilter = new FileNameExtensionFilter("Excel 97 - 2003 Workbook", "xls");
        fileChooser.setFileFilter(fileFilter);
        fileFilter = new FileNameExtensionFilter("Excel Workbook", "xlsx");
        fileChooser.addChoosableFileFilter(fileFilter);
        int j = fileChooser.showSaveDialog(null);
        if (j == JFileChooser.APPROVE_OPTION) {
            if (fileChooser.getFileFilter().getDescription().equals("Excel Workbook")) {
                String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                filePath = filePath.concat(".xlsx");
                File file = new File(filePath);
                XSSFWorkbook workbook = new XSSFWorkbook();
                Sheet sheet = workbook.createSheet();
                Row row = sheet.createRow(0);
                for (int i = 0; i < vectColumn.size(); i++) {
                    Cell cell = row.createCell(i);
                    cell.setCellValue((String)vectColumn.elementAt(i));
                    sheet.autoSizeColumn(i);
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    workbook.write(fileOutputStream);
                    fileOutputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, "File Out Stream error", ex);
                }
            } else {
                String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                filePath = filePath.concat(".xls");
                File file = new File(filePath);
                HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet sheet = workbook.createSheet();
                Row row = sheet.createRow(0);
                for (int i = 0; i < vectColumn.size(); i++) {
                    Cell cell = row.createCell(i);
                    cell.setCellValue((String)vectColumn.elementAt(i));
                    sheet.autoSizeColumn(i);
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    workbook.write(fileOutputStream);
                    fileOutputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(ExcelReaderAndCreator.class.getName()).log(Level.SEVERE, "File Out Stream error", ex);
                }
            }
        }
    }
}
